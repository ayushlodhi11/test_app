class Score < ActiveRecord::Migration
  def change
    create_table :tests do |t|
      t.integer :test_id
      t.integer :student_id

      t.timestamps null: false
    end
    create_table :scores do |t|
      t.integer :test_id
      t.integer :question_id
      t.integer :answer

      t.timestamps null: false
    end
  end
end
