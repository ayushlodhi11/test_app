class Test < ActiveRecord::Migration
  def change
    add_column :tests, :score, :integer
    add_column :tests, :total, :integer
  end
end
