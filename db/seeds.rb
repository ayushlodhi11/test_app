# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

questions = 
[
  [1,"What is Ruby on Rails?","Web application framework","PHP framework","Train loaded with Rubies","None of these",1,1],
  [2,"Who wrote Ruby on Rails?","Gudo Rossum","Yukihiro Matsumoto","David Hansson","None of these",1,1],
  [3,"What does DRY mean?","Don't rub yourself","Don't repeat yourself","Don't redo yourself","None of these",2,1],
  [4,"Which website has been written in Ruby on Rails?","Facebook","Amazon","Twitter","None of these",3,1],
  [5,"Does Ruby on Rails make app development easier?","Yes, it does","No, It doesn't","Kind of","No Idea",1,1],

]
questions.each do |q|
 qes = Question.find_or_initialize_by({:id=>q[0]})
 qes.title = q[1]
 qes.option1 = q[2]
 qes.option2 = q[3]
 qes.option3 = q[4]
 qes.option4 = q[5]
 qes.answer = q[6]
 qes.difficulty = q[7]
 qes.save!
end