class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_filter :authorize
  $DIF = {1=>"Easy", 2=>"Medium",3=>"Hard"}
  def authorize
    if params["action"] == "admin"
      cookies.delete(:email)
    else
      email = cookies[:email]
      if !email.present? and params["action"] != "new" and params["action"] != "create"
        redirect_to "/login" and return if params["action"] != "login" 
      elsif params["action"] == "login"
        page = "/students"
        redirect_to page and return
      end
    end
  end
end
