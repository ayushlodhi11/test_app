class StudentsController < ApplicationController
  before_action :set_student, only: [:show, :edit, :update, :destroy]
  before_action :set_student_only

  def login
    if params["email"].present? and params["password"].present?
      user = Student.find_by(email: params["email"], password: params["password"])
      if user.present?
        cookies["email"] = user.email
        page = "/students"
        redirect_to page and return
      end
      @invalid = true
    end
    render layout: "login"
  end

  def logout
    cookies.delete(:email)
    cookies.delete(:is_admin)
    redirect_to "/" and return
  end
  def addquestion
    q = Question.new({:title=>params["title"],:option1=>params["option1"],:option2=>params["option2"],:option3=>params["option3"],:option4=>params["option4"],:answer=>params["answer"],:difficulty => params["difficulty"]})
    q.save!
    redirect_to "/admin"
  end

  def submittest
    que = question_params
    total = 0;
    score = 0;
    t = Test.create({:student_id => @student.id})
    que.each do|k,v|
      s =  Score.create({:question_id => k, :test_id => t.id, :answer => v})
      total +=1
      score +=1 if s.question.answer == s.answer
    end
    t.update_columns({:score => score,:total =>total})
    redirect_to "/students", :notice => "You have got #{score} out of #{total}"
  end

  def score
    @student = Student.where(:email => cookies[:email]).last
    render layout: false
  end

  def test
    difficulty = params["difficulty"]
    @question = Question.where(:difficulty => difficulty).order("RAND()").limit(10)
  end

  # GET /students
  # GET /students.json
  def index
    @students = Student.all
    render layout: false
  end

  # GET /students/1
  # GET /students/1.json
  def show
  end

  # GET /students/new
  def new
    @student = Student.new
    render layout: "login"
  end

  # GET /students/1/edit
  def edit
  end

  # POST /students
  # POST /students.json
  def create
    @student = Student.new(student_params)

    respond_to do |format|
      if @student.save
        format.html { redirect_to @student, notice: 'Student was successfully created.' }
        format.json { render :show, status: :created, location: @student }
      else
        format.html { render :new }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /students/1
  # PATCH/PUT /students/1.json
  def update
    respond_to do |format|
      if @student.update(student_params)
        format.html { redirect_to @student, notice: 'Student was successfully updated.' }
        format.json { render :show, status: :ok, location: @student }
      else
        format.html { render :edit }
        format.json { render json: @student.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /students/1
  # DELETE /students/1.json
  def destroy
    @student.destroy
    respond_to do |format|
      format.html { redirect_to students_url, notice: 'Student was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student
      @student = Student.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def student_params
      params.require(:student).permit(:name, :email, :password)
    end

    def set_student_only
      @student = Student.find_by(email: cookies["email"])
    end

    def question_params
      params.require(:question)
    end
end
